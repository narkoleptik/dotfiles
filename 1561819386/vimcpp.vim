nnoremap <leader>mu :w<cr>:!make && make upload<cr>
augroup cpp
    au BufRead,BufNewFile *.cpp,*.h,*.c,*.ino
                \ set filetype=cpp shiftwidth=4 softtabstop=4 tabstop=4 expandtab
    setlocal number equalprg=clang-format
    inoremap {<CR> {<CR>}<Esc>ko<tab>
    inoremap [<CR> [<CR>]<Esc>ko<tab>
    inoremap (<CR> (<CR>)<Esc>ko<tab>
    nnoremap <leader>fm gg=G'.``
augroup END
