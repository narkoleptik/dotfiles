augroup go
au BufRead,BufNewFile *.go
      \ set filetype=go shiftwidth=4 softtabstop=4 tabstop=4 expandtab number

nnoremap <leader>c :w<CR>:!go run main.go<CR>
nnoremap <leader>C :w<CR>:!go run main.go

let g:go_fmt_command = "goimports"
augroup END

