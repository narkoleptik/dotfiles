augroup php
  au BufRead,BufNewFile *.php
        \ set filetype=php shiftwidth=2 
        \ softtabstop=2 tabstop=2 expandtab
  " Automatically adds the closing character
  inoremap {<CR> {<CR>}<Esc>ko<tab>
  inoremap [<CR> [<CR>]<Esc>ko<tab>
  inoremap (<CR> (<CR>)<Esc>ko<tab>
  " Formats and reindents the code
  nnoremap <leader>fm gg=G``
  " Saves and reindents
  " Saves and checks for errors
  nnoremap <leader>c :w<CR>:!php % 1>/dev/null<CR>
augroup END
